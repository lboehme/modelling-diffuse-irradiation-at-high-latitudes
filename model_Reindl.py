#This script is the implementation of the Reindl-model.
#An easy model, so nothing special here when compared to the other implemented models

import numpy as np

def model_Reindl(place):
    if place == 'Bergen':
        filename = '6513-wm2_hourly.dat'
    elif place in ['Kiruna', 'Norrkoping','Visby', 'Ny_Alesund', 'Alert', 'Barrow', 'Cape', 'Eureka','Tiksi','Southpole','Lindenberg','Neumayer','Concordia','Southpole_2']:
        filename = '%3s_prepared.txt' % (place)
    else:
        raise ValueError
       
    f = open(filename, 'r')
    lines = f.readlines()
    f.close()
   
    # Make empty lists
    if place == 'Bergen':
        date_list = ['19650401']
        time_list = [0]
        sh_list = [0]
        iex_list = [0]
        ig_list = [0]
        id_list = [0]
        ib_list = [0]
        ib2_list = [0]
    if place == 'Ny_Alesund':
        date_list = ['1993-01-01']
        time_list = [00]
        sh_list = [0]
        iex_list = [0]
        ig_list = [0]
        id_list = [0]
        ib_list = [0]
        ib2_list = [0]
    else:
        date_list = []
        time_list = []
        sh_list = []
        iex_list = []
        ig_list = []
        id_list = []
        ib_list = []
        ib2_list = []
        T2_list = []
        Rh_list = []

    
    #Fill lists with data
    for line in lines:
        if line.startswith('#'):
            continue
        s = line.split()
        date_list.append(str(s[0]))
        time_list.append(int(s[1]))
        sh_list.append(float(s[2]))
        iex_list.append(float(s[4]))
        ig_list.append(float(s[5]))
        id_list.append(float(s[6]))
        ib_list.append(float(s[7]))
        ib2_list.append(float(s[8]))
        if place not in ['Bergen','Ny_Alesund','Eureka', 'Cape','Tiksi']:
            T2_list.append(float(s[10]))
            Rh_list.append(float(s[11]))
        
    
    kd_list = [None] * len(date_list)
    kt_list = [None] * len(date_list)     
    kd_model = [None] * len(kd_list)
    
       
    #Calculate k_t and k_d and fill in their list
    """k_t"""
    for line in range(len(ig_list)):
        try: 
            kt_list[line] = (ig_list[line]/iex_list[line])
        except ZeroDivisionError:
            kt_list[line] = 0
            continue
    
    """k_d"""
    for line in range(len(date_list)):
        try: 
            kd_list[line] = (id_list[line]/ig_list[line])
        except ZeroDivisionError:
            kd_list[line] = 0
            continue
   
#The model, automatically filters out unavailable Temperatore or humidity data, which is measured as 0 or -99
    if place not in ['Bergen','Ny_Alesund','Eureka', 'Cape','Tiksi']:
        for line in range(len(date_list)):
            if (Rh_list[line] in [0, -99.0]) or (T2_list[line] in [0, -99.0]):
                if kt_list[line] <= 0.3:
                    kd_model[line] = 1.020 - 0.254 * kt_list[line] + 0.0123 * np.sin(sh_list[line] * np.pi/180.0)
                elif 0.3 < kt_list[line] < 0.78:
                    kd_model[line] = 1.400 - 1.749 * kt_list[line] + 0.177 * np.sin(sh_list[line] * np.pi / 180.0)
                elif 0.78 <= kt_list[line]:
                    kd_model[line] = 0.486 * kt_list[line] - 0.182 * np.sin(sh_list[line] * np.pi / 180)
            else:
                if kt_list[line] <= 0.3:
                    kd_model[line] = 1.000 - 0.232 * kt_list[line] + 0.0239 * np.sin(sh_list[line] * np.pi/180.0) - 0.000682 * T2_list[line] + 0.0195 * Rh_list[line]/100
                elif 0.3 < kt_list[line] < 0.78:
                    kd_model[line] = 1.329 - 1.716 * kt_list[line] + 0.267 * np.sin(sh_list[line] * np.pi / 180.0) - 0.00357 * T2_list[line] + 0.106 * Rh_list[line]/100
                elif 0.78 <= kt_list[line]:
                    kd_model[line] = 0.426 * kt_list[line] - 0.256 * np.sin(sh_list[line] * np.pi / 180) + 0.00349 * T2_list[line] + 0.0734 * Rh_list[line]/100
    else:
        for line in range(len(date_list)):
            if kt_list[line] <= 0.3:
                kd_model[line] = 1.020 - 0.254 * kt_list[line] + 0.0123 * np.sin(sh_list[line] * np.pi/180.0)
            elif 0.3 < kt_list[line] < 0.78:
                kd_model[line] = 1.400 - 1.749 * kt_list[line] + 0.177 * np.sin(sh_list[line] * np.pi / 180.0)
            elif 0.78 <= kt_list[line]:
                kd_model[line] = 0.486 * kt_list[line] - 0.182 * np.sin(sh_list[line] * np.pi / 180)

    filename = '%3s_Reindl_model.txt' % (place)
    f = open(filename, 'w')
	
	#Save the data
    print('#%7s %9s %9s %9s %9s %9s %10s %9s %9s %9s %9s' % ('Date', 'Time', 'SH', 'I_EX', 'I_G', 'I_D', 'I_B', 'I_B2', 'k_t', 'k_d,meas', 'k_d,mod'), file=f)
    for line in range(len(date_list)):
        output = '%8s %9i %9.3f %9.2f %9.2f %9.2f %10.2f %9.2f %9.5f %9.5f %9.5f' % (date_list[line], time_list[line], sh_list[line], iex_list[line], ig_list[line], id_list[line], ib_list[line], ib2_list[line], kt_list[line], kd_list[line], kd_model[line])
        print(output, file=f)
    
    f.close()
    print('File saved!')
    
    
    
    return