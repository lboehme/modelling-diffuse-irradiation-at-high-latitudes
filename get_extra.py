#This script calculates the equation of time, the apparent solar time AST and the extraterrestrial radiation for a given location.
#It is one of the earlier scripts. A more extensive use of numpy would quicken things up and make the code more readable.

import numpy as np
import datetime

#Read in the data
f = open('Visby_corrected.txt', 'r')
lines = f.readlines()
f.close()

date_list = []
time_list = []
sh_list = []
az_list = []
ig_list = []
id_list = []
ib_list = []
ib2_list = []
kt_list = []
kd_list = []
date2_list = []
yday_list = []
Rh_list = []
T2_list = []

for line in lines:
    if line.startswith("%"):
        continue
    s = line.split()
    date_list.append(datetime.datetime(*map(int, s[0:4])))
    sh_list.append(float(s[5]))
    az_list.append(float(s[4]))
    ig_list.append(float(s[23]))
    id_list.append(float(s[19]))
    ib_list.append(float(s[13]))
    time_list.append(float(s[3]))
    Rh_list.append(float(s[11]))
    T2_list.append(float(s[9]))
    
kt_list = [None]*len(ig_list)
#ib2_list is needed because of a convention for my data type I started with the first script. There is no need for it, but unfortunately it is here.
ib2_list = ib_list.copy()
for i in range(len(sh_list)): 
    yday_list.append((date_list[i].timetuple()).tm_yday)
    
#Checking relative humidity and temperature, filling gaps by interpolating.
def fill_gap(x_list, var):    
    for i in range(len(x_list)):
        if (x_list[i] == 0) or (x_list[i] == -99.0):
            begin_gap = i
            for k in range(len(x_list) - begin_gap):
                if x_list[begin_gap + k] not in [0,-99.9]:
                    end_gap = begin_gap + k
                    i = end_gap
                    break
                if ((begin_gap + k) == len(x_list)-1):
                    end_gap = begin_gap + k
            if (end_gap - begin_gap) < var:
                for l in range(begin_gap, end_gap):
                    x_list[l] = np.round(((end_gap - l) * x_list[begin_gap - 1] + (l - begin_gap) * x_list[end_gap])/(end_gap - begin_gap), decimals=1)
            else:
                for l in range(begin_gap, end_gap):
                    x_list[l] = -99.0
                    
    return x_list
   
Rh_list = np.array(Rh_list)
T2_list = np.array(T2_list)         
    
Rh_list = fill_gap(Rh_list, 36)
T2_list = fill_gap(T2_list, 6)
    

    


#Calulating the solar declination
nday = np.array(yday_list)
w = (2*np.pi)/365.2425

    
#Equation of Time
T = (2*np.pi/365.2425) * (nday-1)
ET = (24/(2*np.pi)) * (0.000075 + 0.01868*np.cos(T) - 0.032077*np.sin(T) - 0.014615*np.cos(2*T) - 0.04089*np.sin(2*T))
 
#Insert Longitude and Latitude of the station here
Long = (Longitude*2*np.pi)/360 ###########################
Lat = (Latiude*2*np.pi)/360    #########################
Lst = (15 * round(Long/((15*2*np.pi)/360)) * 2 * np.pi)/360
Lcorr = (24/(2*np.pi)) * (Lst - Long)

time = np.array(time_list) 

#Change the +1 according to the time zone dependent on the given time_list (if UTC or local time)
AST = (time+1) + Lcorr + ET


#Extraterrestrial radiation
sh = np.array(sh_list)*2*np.pi/360
zenith = (np.pi/2) - sh
E0 = 1.00011 + 0.034221*np.cos(T) + 0.00128*np.sin(T) + 0.000719*np.cos(2*T) + 0.000077*np.sin(2*T)
I0 = 1366.1
iex_list = (E0*I0)*np.cos(zenith)

#If something went wrong: Put a zero there
for i in range(len(iex_list)):
    if iex_list[i] < 0:
        iex_list[i] = 0


#Save the data. Enter the stations name here
filename = '%6s_prepared.txt' % ('Station')
    
#Make new output of all lists
f = open(filename, 'w')
#print('#', file=f)
print('#%9s %5s %7s %10s %10s %10s %10s %10s %10s %9s %10s %10s' % ('Date', 'Time', 'SH', 'AZ', 'I_EX', 'I_G', 'I_D', 'I_B', 'I_B2', 'AST', 'T2', 'Rh'), file=f)
#print('#%18s %18s' % ('-'*15,'-'*15), file=f)
for line in range(len(date_list)):
    output = '%15s %9.3f %9.3f %10.5f %10.5f %10.5f %10.5f %10.5f %10.6f %10.3f %10.3f' % (date_list[line].isoformat(sep=' ',timespec='hours'), sh_list[line], az_list[line], iex_list[line], ig_list[line], id_list[line], ib_list[line], ib2_list[line], AST[line], T2_list[line], Rh_list[line])
    print(output, file=f)

f.close()
print('File saved!')