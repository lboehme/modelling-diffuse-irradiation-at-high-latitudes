#This script plots the mean average error and the Nash-Sutcliffe coefficient E against the latitude. 


import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
mpl.rcdefaults()

#All normal models
mpl.rcParams['lines.linewidth'] = 3
mpl.rcParams['font.size'] = 22
dat = np.loadtxt('Comp_stations_lat.txt', unpack=True)
dat[0] = np.abs(dat[0])

t = plt.figure(figsize=(20,15))
plt.plot(dat[0],dat[1],'bx')
plt.plot(dat[0],dat[1],'b--', label='Reindl')
plt.plot(dat[0],dat[2],'cx')
plt.plot(dat[0],dat[2],'c--', label='S&O-A')
plt.plot(dat[0],dat[3],'gx')
plt.plot(dat[0],dat[3],'g--', label='BRL-Bay')
plt.plot(dat[0],dat[4],'yx')
plt.plot(dat[0],dat[4],'y--', label='S&O')
plt.axis([50,93,0.045,0.320])
plt.xlabel('Latitude [°]')
plt.ylabel('Mean average error')
plt.legend(loc=2)    
t.savefig("MAE_Lat_all.png", bbox_inches='tight')    




u = plt.figure(figsize=(20,15))
plt.plot(dat[0],dat[5],'bx')
plt.plot(dat[0],dat[5],'b--', label='Reindl')
plt.plot(dat[0],dat[6],'cx')
plt.plot(dat[0],dat[6],'c--', label='S&O-A')
plt.plot(dat[0],dat[7],'gx')
plt.plot(dat[0],dat[7],'g--', label='BRL-Bay')
plt.plot(dat[0],dat[8],'yx')
plt.plot(dat[0],dat[8],'y--', label='S&O')
plt.axis([50,93,-0.34,0.95])
plt.xlabel('Latitude [°]')
plt.ylabel('Nash-Sutcliffe coefficient E')
plt.legend(loc=3)
u.savefig("E_Lat_all.png", bbox_inches='tight')   


#All incl. BRL-H-L on control
dat = np.loadtxt('Comp_stations_control.txt', unpack=True)
dat[0] = np.abs(dat[0])

t = plt.figure(figsize=(20,15))
plt.plot(dat[0],dat[1],'bx')
plt.plot(dat[0],dat[1],'b--', label='Reindl')
plt.plot(dat[0],dat[2],'rx')
plt.plot(dat[0],dat[2],'r--', label='BRL-H-L')
plt.plot(dat[0],dat[3],'gx')
plt.plot(dat[0],dat[3],'g--', label='BRL-Bay')
plt.plot(dat[0],dat[4],'yx')
plt.plot(dat[0],dat[4],'y--', label='S&O')
plt.plot(dat[0],dat[5],'cx')
plt.plot(dat[0],dat[5],'c--', label='S&O-A')
plt.axis([55,80,0.045,0.320])
plt.xlabel('Latitude [°]')
plt.ylabel('Mean average error')
plt.legend(loc=2)    
t.savefig("MAE_Lat_control.png", bbox_inches='tight')    




u = plt.figure(figsize=(20,15))
plt.plot(dat[0],dat[6],'bx')
plt.plot(dat[0],dat[6],'b--', label='Reindl')
plt.plot(dat[0],dat[7],'rx')
plt.plot(dat[0],dat[7],'r--', label='BRL-H-L')
plt.plot(dat[0],dat[8],'gx')
plt.plot(dat[0],dat[8],'g--', label='BRL-Bay')
plt.plot(dat[0],dat[9],'yx')
plt.plot(dat[0],dat[9],'y--', label='S&O')
plt.plot(dat[0],dat[10],'cx')
plt.plot(dat[0],dat[10],'c--', label='S&O-A')
plt.axis([55,80,-0.34,0.95])
plt.xlabel('Latitude [°]')
plt.ylabel('Nash-Sutcliffe coefficient E')
plt.legend(loc=3)
u.savefig("E_Lat_control.png", bbox_inches='tight')