#This script fits the BRL-H-L model to randomly selected data from all stations.




import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
import random

    # Make empty lists
date_list = []
time_list = []
sh_list = []
#az_list = []
iex_list = []
ig_list = []
id_list = []
kd_list = []
kt_list = []     
per_list = [] 
KT_list = []
AST_list = []
lat_list = []
    
lat_list_all = np.array([60.39, 67.84, 58.58, 57.67, 78.93, 82.49, 71.32, 79.27, 79.99, 89.98, 71.59,52.21])
lat_list_all = lat_list_all*np.pi/180
place_list = ['Bergen', 'Kiruna', 'Norrkoping', 'Visby', 'Ny_Alesund', 'Alert', 'Barrow','Cape','Eureka','Southpole','Tiksi','Lindenberg']


#Add all stations in one list
for place in place_list:
    if place == 'Bergen':
        filename = '6513-wm2_hourly.dat'
        i = place_list.index(place)
    else:
        filename = '%3s_prepared.txt' % (place)
        i = place_list.index(place)
    
    f = open(filename, 'r')
    lines = f.readlines()
    f.close()

        
	#Fill lists with data
    for line in lines:
        if line == lines[0]:
            if i == 0:
                date = [19650401]
                time = [0]
                sh = [0]
                #az = [0]
                iex = [0]
                ig = [0]
                id = [0]
                AST = [0]
            elif i == 4:
                date = ['1993-01-01']
                time = [00]
                sh = [0]
                iex = [0]
                ig = [0]
                id = [0]
                AST = [0.22048]
            else:
                date = []
                time = []
                sh = []
                #az = []
                iex = []
                ig = []
                id = []
                AST = []
        elif line.startswith("#") or line.startswith("'%"):
            continue
        else:
            s = line.split()
            date.append(str(s[0]))
            time.append(int(s[1]))
            sh.append(float(s[2]))
            iex.append(float(s[4]))
            ig.append(float(s[5]))
            id.append(float(s[6]))
            if i == 0:
                AST.append(float(s[1])%24)
            else:
                AST.append(float(s[9])%24)


        
    


   
    #Calculate k_t and k_d and fill in their list
    """k_t"""
    kt = [None] * len(ig)
    for line in range(len(ig)):
        try: 
            kt[line] = (ig[line]/iex[line])
        except ZeroDivisionError:
            kt[line] = 0
            continue
    #kt_list.append(kt)
    
    """k_d"""
    kd = [None] * len(ig)
    for line in range(len(date)):
        try: 
            kd[line] = (id[line]/ig[line])
        except ZeroDivisionError:
            kd[line] = 0
            continue
    #kd_list.append(kd)
      
    """Persistence"""
    per = [None] * len(ig)
    
    per[0] = 0
    per[-1] = 0
    for line in range(1,len(date)-1):
        if (ig[line-1] == 0) and (ig[line] > 0):
            per[line] = kt[line+1]
        elif (ig[line+1] == 0) and (ig[line] > 0):
            per[line] = kt[line-1]
        elif (ig[line-1] > 0) and (ig[line+1] > 0):
            per[line] = (kt[line-1] + kt[line+1])/2
        else:
            per[line] = 0
    #per_list.append(per)
            
    """Daily clearness"""
    KT = [None] * len(ig)
    if AST[0] >= 1:
        for k in range(24):
            if AST[k] < k:
                ending = k
                break
        for k in range(ending):
            try: 
                KT[k] = sum(ig[0:ending])/sum(iex[0:ending])
            except ZeroDivisionError:
                KT[k] = 0
    for line in range(len(date)):
        if AST[line] <= 1.02:
            try:
                KT_ = sum(ig[line:line+24])/sum(iex[line:line+24])
            except ZeroDivisionError:
                KT_ = 0
            for k in range(24):
                try:
                    KT[line+k] = KT_
                    if k == 23:
                        line += 23
                except IndexError:
                    continue
                
    

	#Filter the data 

    date_corrected, time_corrected, sh_corrected, iex_corrected, ig_corrected, id_corrected, kt_corrected, kd_corrected, KT_corrected, per_corrected, AST_corrected = zip(*((date1, time1, sh1, iex1, ig1, id1, kt1, kd1, KT1, per1, AST1) for date1, time1, sh1, iex1, ig1, id1, kt1, kd1, KT1, per1, AST1 in zip(date, time, sh, iex, ig, id, kt, kd, KT, per, AST) if (ig1 >= 5) and (0 <= per1 <= 1) and (0 <= KT1 <= 1) and (0 <= kt1 <= 1) and (0 < kd1 <= 1) and (sh1 >= 7) and (id1/iex1 <= 0.8) ))
    
	#Save the filtered data in a new list and then do the same for the next station
    date_list.append(date_corrected)
    time_list.append(time_corrected)
    sh_list.append(sh_corrected)
    iex_list.append(iex_corrected)
    ig_list.append(ig_corrected)
    id_list.append(id_corrected)
    kt_list.append(kt_corrected)
    kd_list.append(kd_corrected)
    per_list.append(per_corrected)
    KT_list.append(KT_corrected)
    AST_list.append(AST_corrected)
    
    lat_list1 = [lat_list_all[i]]*len(time_corrected)
    lat_list.append(lat_list1)



 
#Read the amount of data for every station and take the 3rd lowest amount 
lengths = [len(id_list[i]) for i in range(len(id_list))]   
#minlength = min(lengths)
minlength = sorted(lengths)[2]


#Take random data from every station equaling the minimum length chosen before
popt = np.zeros(shape=(2,16))
popt[0] = [-5.38, 6.63, -0.007, 1.75, 1.31, 0, 0, 0, 0, 0, 0, 0, 0,0,0,0]
for k in range(len(popt)-1):
    sample = [random.sample(range(len(id_list[i])), min(minlength,len(id_list[i]))) for i in range(len(id_list))]
    
    kt_train = []
    kt_test = []
    for y in range(len(id_list)):
        for x in range(len(kt_list[y])):
            kt_test.append(kt_list[y][x])
        for x in range(min(minlength,len(kt_list[y]))):
            kt_train.append(kt_list[y][sample[y][x]])    
            
     
    kd_train = []
    kd_test = []
    for y in range(len(id_list)):
        for x in range(len(kd_list[y])):
            kd_test.append(kd_list[y][x])
        for x in range(min(minlength,len(kd_list[y]))):
            kd_train.append(kd_list[y][sample[y][x]])
            
            
    AST_train = []
    AST_test = []
    for y in range(len(id_list)):
        for x in range(len(AST_list[y])):
            AST_test.append(AST_list[y][x])
        for x in range(min(minlength,len(AST_list[y]))):
            AST_train.append(AST_list[y][sample[y][x]])
            
    sh_train = []
    sh_test = []
    for y in range(len(id_list)):
        for x in range(len(sh_list[y])):
            sh_test.append(sh_list[y][x])
        for x in range(min(minlength,len(sh_list[y]))):
            sh_train.append(sh_list[y][sample[y][x]])
    
    KT_train = []
    KT_test = []
    for y in range(len(id_list)):
        for x in range(len(KT_list[y])):
            KT_test.append(KT_list[y][x])
        for x in range(min(minlength,len(KT_list[y]))):
            KT_train.append(KT_list[y][sample[y][x]])
            
    per_train = []
    per_test = []
    for y in range(len(id_list)):
        for x in range(len(per_list[y])):
            per_test.append(per_list[y][x])
        for x in range(min(minlength,len(per_list[y]))):
            per_train.append(per_list[y][sample[y][x]])
            
    lat_train = []
    lat_test = []
    for y in range(len(id_list)):
        for x in range(len(lat_list[y])):
            lat_test.append(lat_list[y][x])
        for x in range(min(minlength,len(lat_list[y]))):
            lat_train.append(lat_list[y][sample[y][x]])
            
            
    
    ydata = np.array(kd_train)
    #ydata = ydata
    xdata = np.array([kt_train, AST_train, sh_train, KT_train, per_train, lat_train])
    
	#The fit function
    def f(dat, b0, b1, b2, b3, b4,b5,b6,b7,b8,b9,b10,b11,b12,b13,b14,b15):
        mod = np.zeros(len(dat[0]))
        for m in range(len(dat[0])):
            if 0.25 < dat[0][m] <= 0.8:
                mod[m] = 1/(1 + np.exp(b0 + b1 * dat[0][m] + b2 * dat[3][m] + b3 * dat[4][m] + b13 * dat[5][m]))
            elif 0.8 < dat[0][m]:
                mod[m] = 1/(1 + np.exp(b4 + b5 * dat[0][m]+ b6 * dat[2][m] + b7 * dat[3][m] + b8 * dat[4][m] + b14 * dat[5][m]))
            elif dat[0][m] <= 0.25:
                mod[m] = 1/(1 + np.exp(b9 + b10 * dat[0][m]  + b11 * dat[3][m] + b12 * dat[4][m] + b15 * dat[5][m]))

        return mod
    

    #The fit command
    popt[k+1], pcov = curve_fit(f, xdata, ydata, popt[k])


#Some statistics for the fit
def cov2cor(X):
    sigma = np.sqrt(np.diag(X))
    sigma_invmat = np.outer(1/sigma, 1/sigma)
    cor = sigma_invmat * X
    return cor


psigma = np.diag(pcov)**0.5 #Standardabweichung
pp = zip(np.arange(16), popt[1], psigma, psigma/popt[1]*100)

#Print the fit parameters
print('\n Fit parameters:')
for l in pp:
    print('b%2i %14.8f +- %10.8f (%8.3f%%)' % l)
    
#Print the correlation matrix for the fit
cor = cov2cor(pcov)
print('\n Correlation matrix:')
for l in cor:
    print(('%12.6f ' * len(l)) % tuple(l))   