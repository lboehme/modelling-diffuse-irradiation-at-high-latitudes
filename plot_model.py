#This script can plot the data from the models

import numpy as np
import matplotlib.pyplot as plt
plt.ioff()
plt.rcParams.update({'font.size': 22})
import datetime
def plot_model(place,file):
    if file == 'S&O':
        name = 'skartveit_model'
    elif file == 'S&O_A':
        name = 'skartveit_model_albedo'
    elif file == 'BRL_Fit':
        name = 'BRL_model_Fit'
    elif file == 'BRL_Bay':
        name = 'BRL_model_Bay'
    elif file == 'BRL_Min':
        name = 'BRL_model_Min'
    elif file == 'BRL_Min2':
        name = 'BRL_model_Min2'
    elif file == 'BRL_Min3':
        name = 'BRL_model_Min3'
    elif file == 'BRL_Lat':
        name = 'BRL_model_Lat'
    elif file == 'Dirint':
        name = 'Dirint_model'
    elif file == 'Disc':
        name = 'Disc_model'
    elif file == 'Reindl':
        name = 'Reindl_model'
    else:
        raise ValueError
    filename = '%3s_%4s.txt' % (place, name)
            
    f = open(filename, 'r')
    lines = f.readlines()
    f.close()
    
    date_list = []
    time_list = []
    sh_list = []
    iex_list = []
    ig_list = []
    id_list = []
    ib_list = []
    ib2_list = []
    kt_list = []
    kd_list = []
    kd_model = []
    date2_list = []
    
    
    for line in lines:
        if line.startswith('#'):
            continue
        s = line.split()
        date_list.append(str(s[0]))
        time_list.append(int(s[1]))
        sh_list.append(float(s[2]))
        iex_list.append(float(s[3]))
        ig_list.append(float(s[4]))
        id_list.append(float(s[5]))
        ib_list.append(float(s[6]))
        ib2_list.append(float(s[7]))
        kt_list.append(float(s[8]))
        kd_list.append(float(s[9]))
        kd_model.append(float(s[10]))
    
    if place == 'Bergen':
        for line in range(len(date_list)):
            date2_list.append(datetime.datetime.strptime((date_list[line]), "%Y%m%d"))
    else:
         for line in range(len(date_list)):
            date2_list.append(datetime.datetime.strptime((date_list[line]), "%Y-%m-%d"))   
      
#First, filter the data
    sh_list_corrected, iex_list_corrected, ig_list_corrected, id_list_corrected, kt_list_corrected, kd_list_corrected, kd_model_corrected = zip(*((sh, iex, ig, id1, kt, kd, kdm) for sh, iex, ig, id1, kt, kd, kdm in zip(sh_list, iex_list, ig_list, id_list, kt_list, kd_list, kd_model) if (ig >= 5) and (0 <= kt <= 1) and (0 < kd <= 1) and (0 < kdm <= 1) and (sh >= 7) and (id1/iex <= 0.8)))
    kt_corrected = np.array(kt_list_corrected)
    kd_corrected = np.array(kd_list_corrected)
    kdm_corrected = np.array(kd_model_corrected)
	
#And then plot it
    t = plt.figure(figsize=(20,15))
    plt.plot(kt_corrected,kd_corrected, 'o', markerfacecolor='none', markeredgecolor='black', label = 'Observed')
    plt.plot(kt_corrected,kdm_corrected, 'o', markerfacecolor='none', markeredgecolor='red', label = 'Model %3s' % (file))
    plt.axis([-0.05,1.2,-0.05,1.2])
    plt.xlabel('Clearness index')
    plt.ylabel('Diffuse index')
    plt.legend(title = 'Only corrected data, datapoints = %6i' % (len(kd_corrected)))    
    t.savefig("%3s_%3s_kt_kd_corrected.png" % (place, file), bbox_inches='tight')

    print('Fig. saved')
 
    return 