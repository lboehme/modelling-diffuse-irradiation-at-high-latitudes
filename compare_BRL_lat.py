#This script allows to analyse the BRL-H-L model with fixed parameters


import numpy as np
import matplotlib.pyplot as plt
plt.rcParams.update({'font.size': 22})

#The fitted parameters
b = [-3.19770206,  6.67977243,  1.61676494,  1.06015204, -3.67943995, -1.00434543,  0.03328069,  1.64218568,  1.64891401, -6.12479022, -5.49821597,  0.61535358,  3.47724122, -2.04517801,  1.64311886, 2.23147351]

kt_list = []
KT_list = []
per_list = []
lat = []
sh_list = []

#kt = x-axis, and the fixed parameters
kt_list = np.arange(1,1000)/1000
sh_list = [20]*999
KT_list = [0.5]*999
per_list = [0.5]*999

#The BRL-H-L model
def f(dat, b0, b1, b2, b3, b4,b5,b6,b7,b8,b9,b10,b11,b12,b13,b14,b15):
    mod = np.zeros(len(dat[0]))
    for m in range(len(dat[0])):
        if 0.25 < dat[0][m] <= 0.8:
            mod[m] = 1/(1 + np.exp(b0 + b1 * dat[0][m] + b2 * dat[3][m] + b3 * dat[4][m] + b13 * dat[5][m]))
        elif 0.8 < dat[0][m]:
            mod[m] = 1/(1 + np.exp(b4 + b5 * dat[0][m]+ b6 * dat[2][m] + b7 * dat[3][m] + b8 * dat[4][m] + b14 * dat[5][m]))
        elif dat[0][m] <= 0.25:
            mod[m] = 1/(1 + np.exp(b9 + b10 * dat[0][m]  + b11 * dat[3][m] + b12 * dat[4][m] + b15 * dat[5][m]))
    
    return mod

#The model only takes one array as input => Combine the data to one.
dat50 = np.array([kt_list, [0]*999, sh_list, KT_list, per_list, [50*np.pi/180]*999])
dat60 = np.array([kt_list, [0]*999, sh_list, KT_list, per_list, [60*np.pi/180]*999])
dat70 = np.array([kt_list, [0]*999, sh_list, KT_list, per_list, [70*np.pi/180]*999])
dat80 = np.array([kt_list, [0]*999, sh_list, KT_list, per_list, [80*np.pi/180]*999])
dat90 = np.array([kt_list, [0]*999, sh_list, KT_list, per_list, [90*np.pi/180]*999])

#Modelling the values
kd50 = f(dat50, *b)
kd60 = f(dat60,*b)
kd70 = f(dat70,*b)
kd80 = f(dat80,*b)
kd90 = f(dat90,*b)

#The plot
p = plt.figure(figsize=(20,15))
plt.plot(kt_list,kd50, 'o', color='black', label = 'Lat = 50deg')
plt.plot(kt_list,kd60, 'o', color='maroon', label = 'Lat = 60deg')
plt.plot(kt_list,kd70, 'o', color='purple', label = 'Lat = 70deg')
plt.plot(kt_list,kd80, 'o', color='darkgreen', label = 'Lat = 80deg')
plt.plot(kt_list,kd90, 'o', color='midnightblue', label = 'Lat = 90deg')
plt.axis([-0.05,1.2,-0.05,1.2])
plt.xlabel('Clearness index')
plt.ylabel('Diffusion index')
plt.legend(title = 'h = 20°, Kt=0.5,per=0.5')
p.savefig("BRL_Lat_compare.png", bbox_inches='tight')