#This script gives the statistical parameters used in the thesis
import numpy as np
import datetime
import sys
import matplotlib.pyplot as plt
np.seterr(divide='ignore', invalid='ignore')

#Definitions of the five parameters
def MBE(a,am):
    return sum(a-am)/len(a)

def MAE(a,am):
    return sum(abs(a-am))/len(a)

def R2(a,am):
    a_mean = sum(a)/len(a)
    am_mean = sum(am)/len(am)
    z = sum((a-a_mean)*(am-am_mean))
    x = sum((a-a_mean)**2)
    y = sum((am-am_mean)**2)
    return (z/(x*y)**0.5)**2

def E(a,am):
    a_mean = sum(a)/len(a)
    return 1 - ( sum((a-am)**2)/sum((a-a_mean)**2) )

def Pd(a,am):
    dif = abs((a-am)/a)
    val = (dif <= 0.1).sum()
    return (val/len(a))*100

#The function to call for calculating them
def statistics(place, file):
    if file == 'S&O':
        name = 'skartveit_model'
    elif file == 'BRL_Fit':
        name = 'BRL_model_Fit'
    elif file == 'BRL_Bay':
        name = 'BRL_model_Bay'
    elif file == 'BRL_Min':
        name = 'BRL_model_Min'
    elif file == 'BRL_Min2':
        name = 'BRL_model_Min2'
    elif file == 'BRL_Min3':
        name = 'BRL_model_Min3'
    elif file == 'BRL_Lat':
        name = 'BRL_model_Lat'
    elif file == 'Reindl':
        name = 'Reindl_model'
    filename = '%3s_%4s.txt' % (place, name)

    f = open(filename, 'r')
    lines = f.readlines()
    f.close()
    
    date_list = []
    time_list = []
    sh_list = []
    iex_list = []
    ig_list = []
    id_list = []
    ib_list = []
    ib2_list = []
    kt_list = []
    kd_list = []
    kd_model = []
    date2_list = []
    
    for line in lines:
        if line.startswith('#'):
            continue
        s = line.split()
        date_list.append(str(s[0]))
        time_list.append(int(s[1]))
        sh_list.append(float(s[2]))
        iex_list.append(float(s[3]))
        ig_list.append(float(s[4]))
        id_list.append(float(s[5]))
        ib_list.append(float(s[6]))
        ib2_list.append(float(s[7]))
        kt_list.append(float(s[8]))
        kd_list.append(float(s[9]))
        kd_model.append(float(s[10]))
        
    if place == 'Bergen':
        for line in range(len(date_list)):
            date2_list.append(datetime.datetime.strptime((date_list[line]), "%Y%m%d"))
    else:
         for line in range(len(date_list)):
            date2_list.append(datetime.datetime.strptime((date_list[line]), "%Y-%m-%d")) 
        
    """All data"""
    kd = np.array(kd_list)
    kdm = np.array(kd_model)
    
    """Only April - October"""
    date_list_summer, kt_list_summer, kd_list_summer, kd_model_summer = zip(*((date, kt, kd, kdm) for date, kt, kd, kdm in zip(date2_list, kt_list, kd_list, kd_model) if (date.month == 4) or (date.month == 5) or (date.month == 6) or (date.month == 7) or (date.month == 8) or (date.month == 9) or (date.month == 10)))
    
    kd_summer = np.array(kd_list_summer)
    kdm_summer = np.array(kd_model_summer)
    
    """Only November - March"""
    date_list_winter, kt_list_winter, kd_list_winter, kd_model_winter = zip(*((date, kt, kd, kdm) for date, kt, kd, kdm in zip(date2_list, kt_list, kd_list, kd_model) if (date.month == 11) or (date.month == 12) or (date.month == 1) or (date.month == 2) or (date.month == 3) ))
    
    kd_winter = np.array(kd_list_winter)
    kdm_winter = np.array(kd_model_winter)
    
    """Only I_G >= 5 and some more"""
    sh_list_corrected, ig_list_corrected, id_list_corrected, kt_list_corrected, kd_list_corrected, kd_model_corrected = zip(*((sh, ig, id, kt, kd, kdm) for sh, ig, id, kt, kd, kdm in zip(sh_list, ig_list, id_list, kt_list, kd_list, kd_model) if (ig >= 5) and (0 <= kt <= 1) and (0 < kd <= 1) and (sh >= 7) and (id/ig <= 1.1) and (0.0 <= kdm <= 1.0) ))
    
    kd_corrected = np.array(kd_list_corrected)
    kdm_corrected = np.array(kd_model_corrected)
    
    """Only I_G >= 5 and some more and summer"""
    date_list_sumcor, sh_list_sumcor, ig_list_sumcor, id_list_sumcor, kt_list_sumcor, kd_list_sumcor, kd_model_sumcor = zip(*((date, sh, ig, id, kt, kd, kdm) for date, sh, ig, id, kt, kd, kdm in zip(date2_list, sh_list, ig_list, id_list, kt_list, kd_list, kd_model) if (ig >= 5) and (0 <= kt <= 1) and (0 < kd <= 1) and (sh >= 7) and (id/ig <= 1.1) and ((date.month == 4) or (date.month == 5) or (date.month == 6) or (date.month == 7) or (date.month == 8) or (date.month == 9) or (date.month == 10)) ))
    
    kd_sumcor = np.array(kd_list_sumcor)
    kdm_sumcor = np.array(kd_model_sumcor)
    
    """Only I_G >= 5 and some more and winter"""
    date_list_wincor, sh_list_wincor, ig_list_wincor, id_list_wincor, kt_list_wincor, kd_list_wincor, kd_model_wincor = zip(*((date, sh, ig, id, kt, kd, kdm) for date, sh, ig, id, kt, kd, kdm in zip(date2_list, sh_list, ig_list, id_list, kt_list, kd_list, kd_model) if (ig >= 5) and (0 <= kt <= 1) and (0 < kd <= 1) and (sh >= 7) and (id/ig <= 1.1) and ((date.month == 11) or (date.month == 12) or (date.month == 1) or (date.month == 2) or (date.month == 3) ) ))
    
    kd_wincor = np.array(kd_list_wincor)
    kdm_wincor = np.array(kd_model_wincor)
    
    orig_stdout = sys.stdout
    f = open('Statistical_analysis_%5s_%3s.txt' % (place, file), 'w')
    sys.stdout = f
    
    print('#All data')
    a = kd
    am = kdm
    print('#%9s %9s %9s %9s %9s' % ('MBE', 'MAE', 'R2', 'E', 'Pd'))
    #print('#%18s %18s' % ('-'*15,'-'*15), file=f)
    output = '%9.3f %9.3f %9.8f %9.3f %9.3f %9i' % (MBE(a,am), MAE(a,am), R2(a,am), E(a,am), Pd(a,am), len(a))
    print(output)  
        
    print('\n#Only summer')
    a = kd_summer
    am = kdm_summer
    print('#%9s %9s %9s %9s %9s' % ('MBE', 'MAE', 'R2', 'E', 'Pd'))
    #print('#%18s %18s' % ('-'*15,'-'*15), file=f)
    output = '%9.3f %9.3f %9.8f %9.3f %9.3f %9i' % (MBE(a,am), MAE(a,am), R2(a,am), E(a,am), Pd(a,am), len(a))
    print(output)  
    
    print('\n#Only winter')
    a = kd_winter
    am = kdm_winter
    print('#%9s %9s %9s %9s %9s' % ('MBE', 'MAE', 'R2', 'E', 'Pd'))
    #print('#%18s %18s' % ('-'*15,'-'*15), file=f)
    output = '%9.3f %9.3f %9.8f %9.3f %9.3f %9i' % (MBE(a,am), MAE(a,am), R2(a,am), E(a,am), Pd(a,am), len(a))
    print(output)  
        
    print('\n#Corrected')
    a = kd_corrected
    am = kdm_corrected
    print('#%9s %9s %9s %9s %9s' % ('MBE', 'MAE', 'R2', 'E', 'Pd'))
    #print('#%18s %18s' % ('-'*15,'-'*15), file=f)
    output = '%9.3f & %9.3f & %9.8f & %9.3f & %9.3f & %9i & %9i' % (MBE(a,am), MAE(a,am), R2(a,am), E(a,am), Pd(a,am), len(a), len(a)/len(kd)*100)
    print(output)      
        
    print('\n#Corrected and summer')
    a = kd_sumcor
    am = kdm_sumcor
    print('#%9s %9s %9s %9s %9s' % ('MBE', 'MAE', 'R2', 'E', 'Pd'))
    #print('#%18s %18s' % ('-'*15,'-'*15), file=f)
    output = '%9.3f & %9.3f & %9.8f & %9.3f & %9.3f & %9i & %9i' % (MBE(a,am), MAE(a,am), R2(a,am), E(a,am), Pd(a,am), len(a), (len(a)/len(kd))*100)
    print(output)   
        
    print('\n#Corrected and winter')
    a = kd_wincor
    am = kdm_wincor
    print('#%9s %9s %9s %9s %9s' % ('MBE', 'MAE', 'R2', 'E', 'Pd'))
    #print('#%18s %18s' % ('-'*15,'-'*15), file=f)
    output = '%9.3f %9.3f %9.8f %9.3f %9.3f %9i' % (MBE(a,am), MAE(a,am), R2(a,am), E(a,am), Pd(a,am), len(a))
    print(output)       
    
    sys.stdout = orig_stdout
    f.close()
        
    print('Finished!')
        
    return
        
#This gives the measured vs. modelled diffuse index plots
def mvm(place,file,niceplacename):  
    if file == 'S&O':
        name = 'skartveit_model'
    elif file == 'BRL_Fit':
        name = 'BRL_model_Fit'
    elif file == 'BRL_Bay':
        name = 'BRL_model_Bay'
    elif file == 'BRL_Min':
        name = 'BRL_model_Min'
    elif file == 'BRL_Min2':
        name = 'BRL_model_Min2'
    elif file == 'BRL_Min3':
        name = 'BRL_model_Min3'
    elif file == 'BRL_Lat':
        name = 'BRL_model_Lat'
    elif file == 'Reindl':
        name = 'Reindl_model'
    filename = '%3s_%4s.txt' % (place, name)

    f = open(filename, 'r')
    lines = f.readlines()
    f.close()
    
    date_list = []
    time_list = []
    sh_list = []
    iex_list = []
    ig_list = []
    id_list = []
    ib_list = []
    ib2_list = []
    kt_list = []
    kd_list = []
    kd_model = []
    date2_list = []
    
    for line in lines:
        if line.startswith('#'):
            continue
        s = line.split()
        date_list.append(str(s[0]))
        time_list.append(int(s[1]))
        sh_list.append(float(s[2]))
        iex_list.append(float(s[3]))
        ig_list.append(float(s[4]))
        id_list.append(float(s[5]))
        ib_list.append(float(s[6]))
        ib2_list.append(float(s[7]))
        kt_list.append(float(s[8]))
        kd_list.append(float(s[9]))
        kd_model.append(float(s[10]))
        
    if place == 'Bergen':
        for line in range(len(date_list)):
            date2_list.append(datetime.datetime.strptime((date_list[line]), "%Y%m%d"))
    else:
         for line in range(len(date_list)):
            date2_list.append(datetime.datetime.strptime((date_list[line]), "%Y-%m-%d")) 
           
    """Only I_G >= 5 and some more"""
    sh_list_corrected, ig_list_corrected, id_list_corrected, kt_list_corrected, kd_list_corrected, kd_model_corrected = zip(*((sh, ig, id, kt, kd, kdm) for sh, ig, id, kt, kd, kdm in zip(sh_list, ig_list, id_list, kt_list, kd_list, kd_model) if (ig >= 5) and (0 <= kt <= 1) and (0 < kd <= 1) and (0 < kdm <= 1) and (sh >= 7) and (id/ig <= 1.1) ))
    
    kd_corrected = np.array(kd_list_corrected)
    kdm_corrected = np.array(kd_model_corrected) 
    plt.rcParams.update({'font.size': 22})
    q = plt.figure(figsize=(20,15))
    plt.plot(kd_corrected,kdm_corrected,'o',markerfacecolor='none',markeredgecolor='black')
    plt.axis([-0.05,1.2,-0.05,1.2])
    plt.xlabel('Measured diffuse fraction')
    plt.ylabel('Modelled diffuse fraction')        
    plt.legend(title = 'Modelled vs measured diffuse fraction for %3s by %3s' % (niceplacename,file))
    q.savefig("MvM_%3s_%3s.png" % (place, file), bbox_inches='tight')
            
    return

#This gives the corrected statistical parameters for all stations in one file as well as these averaged down to one value for easy comparison of the models.
def statistics_all(file,Bergen='no',control='no'):
    if file == 'S&O':
        name = 'skartveit_model'
    elif file == 'BRL_Fit':
        name = 'BRL_model_Fit'
    elif file == 'BRL_Bay':
        name = 'BRL_model_Bay'
    elif file == 'BRL_Min':
        name = 'BRL_model_Min'
    elif file == 'BRL_Min2':
        name = 'BRL_model_Min2'
    elif file == 'BRL_Min3':
        name = 'BRL_model_Min3'
    elif file == 'BRL_Lat':
        name = 'BRL_model_Lat'
    elif file == 'Reindl':
        name = 'Reindl_model'
    
    corr = np.zeros(shape=(12,6))
    corrected = np.array([0,0,0,0,0,0], dtype='float64')
    sumcor = np.array([0,0,0,0,0,0], dtype='float64')
    wincor = np.array([0,0,0,0,0,0], dtype='float64')
    
    place_Bergen = ['Lindenberg', 'Visby', 'Norrkoping', 'Bergen', 'Kiruna', 'Barrow', 'Tiksi', 'Ny_Alesund', 'Cape', 'Eureka', 'Alert', 'Southpole']
    place_ = ['Norrkoping','Visby','Kiruna', 'Ny_Alesund', 'Alert', 'Barrow', 'Cape', 'Eureka','Tiksi','Southpole']
    place_control = ['Neumayer','Concordia']
    
    if Bergen == 'no':
        place_list = place_
    elif Bergen == 'yes':
        place_list = place_Bergen
    if control == 'yes':
        place_list = place_control
    i=0
    for place in place_list:
        
        filename = '%3s_%4s.txt' % (place, name)
  
        f = open(filename, 'r')
        lines = f.readlines()
        f.close()
    
        date_list = []
        time_list = []
        sh_list = []
        iex_list = []
        ig_list = []
        id_list = []
        ib_list = []
        ib2_list = []
        kt_list = []
        kd_list = []
        kd_model = []
        date2_list = []
    
        for line in lines:
            if line.startswith('#'):
                continue
            s = line.split()
            date_list.append(str(s[0]))
            time_list.append(int(s[1]))
            sh_list.append(float(s[2]))
            iex_list.append(float(s[3]))
            ig_list.append(float(s[4]))
            id_list.append(float(s[5]))
            ib_list.append(float(s[6]))
            ib2_list.append(float(s[7]))
            kt_list.append(float(s[8]))
            kd_list.append(float(s[9]))
            kd_model.append(float(s[10]))
              
        if place == 'Bergen':
            for line in range(len(date_list)):
                date2_list.append(datetime.datetime.strptime((date_list[line]), "%Y%m%d"))
        else:
             for line in range(len(date_list)):
                date2_list.append(datetime.datetime.strptime((date_list[line]), "%Y-%m-%d")) 

        """Only I_G >= 5 and some more"""
        sh_list_corrected, ig_list_corrected, id_list_corrected, kt_list_corrected, kd_list_corrected, kd_model_corrected = zip(*((sh, ig, id, kt, kd, kdm) for sh, ig, id, kt, kd, kdm in zip(sh_list, ig_list, id_list, kt_list, kd_list, kd_model) if (ig >= 5) and (0 <= kt <= 1) and (0 < kd <= 1) and (0 < kdm <= 1) and (sh >= 7) and (id/ig <= 1.1) ))
        
        kd_corrected = np.array(kd_list_corrected)
        kdm_corrected = np.array(kd_model_corrected)
        
        """Only I_G >= 5 and some more and summer"""
        date_list_sumcor, sh_list_sumcor, ig_list_sumcor, id_list_sumcor, kt_list_sumcor, kd_list_sumcor, kd_model_sumcor = zip(*((date, sh, ig, id, kt, kd, kdm) for date, sh, ig, id, kt, kd, kdm in zip(date2_list, sh_list, ig_list, id_list, kt_list, kd_list, kd_model) if (ig >= 5) and (0 <= kt <= 1) and (0 < kd <= 1) and (sh >= 7) and (id/ig <= 1.1) and ((date.month == 4) or (date.month == 5) or (date.month == 6) or (date.month == 7) or (date.month == 8) or (date.month == 9) or (date.month == 10)) ))
        
        kd_sumcor = np.array(kd_list_sumcor)
        kdm_sumcor = np.array(kd_model_sumcor)
        
        """Only I_G >= 5 and some more and winter"""
        date_list_wincor, sh_list_wincor, ig_list_wincor, id_list_wincor, kt_list_wincor, kd_list_wincor, kd_model_wincor = zip(*((date, sh, ig, id, kt, kd, kdm) for date, sh, ig, id, kt, kd, kdm in zip(date2_list, sh_list, ig_list, id_list, kt_list, kd_list, kd_model) if (ig >= 5) and (0 <= kt <= 1) and (0 < kd <= 1) and (sh >= 7) and (id/ig <= 1.1) and ((date.month == 11) or (date.month == 12) or (date.month == 1) or (date.month == 2) or (date.month == 3) ) ))
        
        kd_wincor = np.array(kd_list_wincor)
        kdm_wincor = np.array(kd_model_wincor)
   
        corrected += [MBE(kd_corrected, kdm_corrected), MAE(kd_corrected, kdm_corrected), R2(kd_corrected, kdm_corrected), E(kd_corrected, kdm_corrected), Pd(kd_corrected, kdm_corrected), len(kd_corrected)]
        sumcor += [MBE(kd_sumcor, kdm_sumcor), MAE(kd_sumcor, kdm_sumcor), R2(kd_sumcor, kdm_sumcor), E(kd_sumcor, kdm_sumcor), Pd(kd_sumcor, kdm_sumcor), len(kd_sumcor)]
        wincor += [MBE(kd_wincor, kdm_wincor), MAE(kd_wincor, kdm_wincor), R2(kd_wincor, kdm_wincor), E(kd_wincor, kdm_wincor), Pd(kd_wincor, kdm_wincor), len(kd_wincor)]
        corr[i] = [MBE(kd_corrected, kdm_corrected), MAE(kd_corrected, kdm_corrected), R2(kd_corrected, kdm_corrected), E(kd_corrected, kdm_corrected), Pd(kd_corrected, kdm_corrected), len(kd_corrected)]
        i += 1
        
    corrected[0:5] =  corrected[0:5]/len(place_list)
    sumcor[0:5] = sumcor[0:5]/len(place_list)
    wincor[0:5] = wincor[0:5]/len(place_list)
        
    orig_stdout = sys.stdout
    if Bergen == 'no':
        f = open('Statistical_analysis_%3s_ohne_Bergen.txt' % (file), 'w')
    elif Bergen == 'yes':
        f = open('Statistical_analysis_%3s.txt' % (file), 'w')
    if control == 'yes':
        f = open('Statistical_analysis_%3s_control.txt' % (file), 'w')
    sys.stdout = f
    
    print('#Corrected')
    print('#%9s %9s %9s %9s %9s %9s' % ('MBE', 'MAE', 'R2', 'E', 'Pd', '#'))
    #print('#%18s %18s' % ('-'*15,'-'*15), file=f)
    output = '%9.3f & %9.3f & %9.8f & %9.3f & %9.3f & %9i' % (corrected[0], corrected[1], corrected[2], corrected[3], corrected[4], corrected[5])
    print(output)      
        
    print('\n#Corrected and summer')
    print('#%9s %9s %9s %9s %9s %9s' % ('MBE', 'MAE', 'R2', 'E', 'Pd', '#'))
    #print('#%18s %18s' % ('-'*15,'-'*15), file=f)
    output = '%9.3f & %9.3f & %9.8f & %9.3f & %9.3f & %9i' % (sumcor[0], sumcor[1], sumcor[2], sumcor[3], sumcor[4], sumcor[5])
    print(output)   
        
    print('\n#Corrected and winter')
    print('#%9s %9s %9s %9s %9s %9s' % ('MBE', 'MAE', 'R2', 'E', 'Pd', '#'))
    #print('#%18s %18s' % ('-'*15,'-'*15), file=f)
    output = '%9.3f %9.3f %9.8f %9.3f %9.3f %9i' % (wincor[0], wincor[1], wincor[2], wincor[3], wincor[4], wincor[5])
    print(output)       
    
    print('\n########### All places')
    print('#%9s & %9s & %9s & %9s & %9s & %9s & %9s & %9s \\\\ \midrule' % ('Place', 'Lat', 'MBE', 'MAE', 'R2', 'E', 'Pd', '#'))
    for i in range(len(place_list)):
        lat_list = {'Lindenberg': 52.21,'Bergen': 60.39, 'Visby': 57.67, 'Norrkoping': 58.58, 'Kiruna': 67.84, 'Alert': 82.49, 'Barrow': 71.32, 'Cape': 79.27, 'Eureka': 79.99, 'Ny_Alesund': 78.93, 'Southpole': -89.98, 'Tiksi': 71.59, 'Neumayer': -70.65, 'Concordia': -75.10}
        lat = lat_list.get(place_list[i])
        print('%9s & %9.2f & %9.3f & %9.3f & %9.3f & %9.3f & %9.3f & %9i \\\\' % (place_list[i], lat, corr[i,0], corr[i,1], corr[i,2], corr[i,3], corr[i,4], corr[i,5]))      
         
    sys.stdout = orig_stdout
    f.close()
        
    print('Finished!')
        
    return