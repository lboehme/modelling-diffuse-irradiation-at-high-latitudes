#This code file is the implementations of the Skartveit and Olseth model. It is very complicated to understand, so I suggest a look into the thesis explanation for the model.

import numpy as np

def model_Skartveit(place):
#First: Read in the stations measured data
    if place == 'Bergen':
        filename = '6513-wm2_hourly.dat'
    elif place in ['Kiruna', 'Norrkoping','Visby', 'Ny_Alesund', 'Alert', 'Barrow','Eureka','Tiksi','Cape','Southpole','Lindenberg','Neumayer','Concordia']:
        filename = '%3s_prepared.txt' % (place)
    else:
        raise ValueError
    
    f = open(filename, 'r')
    lines = f.readlines()
    f.close()
    
    # Make empty lists
    date_list = []
    time_list = []
    sh_list = []
    az_list = []
    iex_list = []
    ig_list = []
    id_list = []
    ib_list = []
    ib2_list = []
    kt_list = []
    kd_list = []
    o3_list = []
    rho_list = []
    k1_list = []
    
    #Fill lists with data
    for line in lines:
        if line.startswith('#'):
            continue
        s = line.split()
        date_list.append(str(s[0]))
        time_list.append(int(s[1]))
        sh_list.append(float(s[2]))
        az_list.append(float(s[3]))
        iex_list.append(float(s[4]))
        ig_list.append(float(s[5]))
        id_list.append(float(s[6]))
        ib_list.append(float(s[7]))
        ib2_list.append(float(s[8]))
        
        
        
        
    #Calculate k_t and k_d and fill in their list
    """k_t"""
    for line in range(len(ig_list)):
        try: 
            kt_list.append(ig_list[line]/iex_list[line])
        except ZeroDivisionError:
            kt_list.append(0)
            continue
    
    """k_d"""
    for line in range(len(date_list)):
        try: 
            kd_list.append(id_list[line]/ig_list[line])
        except ZeroDivisionError:
            kd_list.append(0)
            continue
    
    
    
    #There is a correction to the model for differing albedo values from the standard snow-free albedo of 0.15. This is a pure code implementation from the original paper.
	#The albedos are rough estimates dependent on the duration of a snow cover at the station.
    albedos = {'Lindenberg': 0.15,'Bergen': 0.2, 'Visby': 0.2, 'Norrkoping': 0.2, 'Kiruna': 0.3, 'Alert': 0.8, 'Barrow': 0.7, 'Cape': 0.8, 'Eureka': 0.7, 'Ny_Alesund': 0.7, 'Southpole': 0.87, 'Tiksi': 0.7, 'Neumayer': 0.87, 'Concordia': 0.87}

    r = albedos[place]
    r_star = 0.15
    
    if (np.abs(r - r_star) > 0.1):
        A = 0.2
        h = np.array(sh_list)
        h_prime = np.copy(h)
        h_prime[:] = 37
        k_r = np.array(kt_list)
        
        def k_1(x):
            k_1 = 0.83 - 0.56 * np.exp(-0.006 * x)
            
            return k_1
        
        k_r_prime = k_r * k_1(h_prime)/k_1(h)
        
        R = (1 - A - k_r_prime)/(1 - k_r_prime * r)
        R[R<0.08] = 0.08
        
        k_r_star = k_r * ((1 - r * R)/(1 - r_star * R))
        
        kt_list = k_r_star
    
    
    
    
    
    
    
    
    
    
    #The standard S&O model
    
    
    """k1"""
    for line in range(len(date_list)):
        try: 
            k1_list.append(0.83-0.56*np.exp(-0.06*sh_list[line]))
        except ZeroDivisionError:
            k1_list.append(0)
            continue
    
    """rho"""
    for line in range(len(date_list)):
        try: 
            rho_list.append(kt_list[line]/k1_list[line])
        except ZeroDivisionError:
            rho_list.append(0)
            continue
    
    """o3"""    
    o3_list = [None] *len(date_list)
    for line in range(len(date_list)):
        if line == 0:
            o3_list[line] = np.abs(rho_list[line]-rho_list[line+1])
            continue
                
        if line == len(date_list)-1:
            o3_list[line] = np.abs(rho_list[line]-rho_list[line-1])
            continue
            
        o3_list[line] = (((rho_list[line]-rho_list[line-1])**2+(rho_list[line]-rho_list[line+1])**2)/2)**0.5
    
    
    
    kd_model = [None] * len(kd_list)
    
    k2_list = []
    for line in range(len(k1_list)):
        k2_list.append(k1_list[line]*0.95)
        
    alpha = []
    for line in range(len(k1_list)):
        if sh_list[line] == 0:
            alpha.append(0)
        else:
            try:
                alpha.append((1/(np.sin(sh_list[line]*np.pi/180))**0.6))
            except:
                alpha.append(0)
                continue
            
    kbmax = []
    for line in range(len(k1_list)):
        kbmax.append(0.81**alpha[line])
        
    d1_list = []
    for line in range(len(k1_list)):
        if sh_list[line] <= 1.4:
            d1_list.append(1)
        else:
            d1_list.append(0.07 + 0.046*((90-sh_list[line])/(sh_list[line]+3)))
        
        
    d2_list = []
    for line in range(len(k1_list)):  
        K2 = 0.5*(1+np.sin(np.pi*((k2_list[line]-0.22)/(k1_list[line]-0.22))-np.pi/2))
        
        d2_list.append(1 - (1-d1_list[line]) * (0.11*np.sqrt(K2) + 0.15*K2 + 0.74*K2**2))
        
    K2 = None
    
    ktmax = []
    for line in range(len(k1_list)):
        ktmax.append( (kbmax[line] + (d2_list[line]*k2_list[line])/(1-k2_list[line])) / (1 + (d2_list[line]*k2_list[line])/(1-k2_list[line])))
        
    kdmax = []
    for line in range(len(k1_list)):
        kdmax.append((d2_list[line]*k2_list[line]*(1-ktmax[line]))/(ktmax[line]*(1-k2_list[line])))
        
    kx_list = []
    for line in range(len(k1_list)):
        kx_list.append(0.56 - 0.32 * np.exp(-0.06 * sh_list[line]))
    
    kl_list = []
    for line in range(len(k1_list)):
        kl_list.append((kt_list[line] - 0.14)/(kx_list[line] - 0.14))
        
    kr_list = []
    for line in range(len(k1_list)):
        kr_list.append((kt_list[line] - kx_list[line])/0.71)
    
    
        """Invariable hours"""
    
    for line in range(len(ig_list)):
        if o3_list[line] <= 0.0000001:
            if kt_list[line] <= 0.22:
                kd_model[line] = 1
                
            elif 0.22 < kt_list[line] <= k2_list[line]:
                K = 0.5*(1+np.sin(np.pi*((kt_list[line]-0.22)/(k1_list[line]-0.22))-np.pi/2))
                
                kd_model[line] = 1 - (1-d1_list[line]) * (0.11*np.sqrt(K) + 0.15*K + 0.74*K**2) 
    
            elif k2_list[line] < kt_list[line] <= ktmax[line]:
                kd_model[line] = (d2_list[line]*k2_list[line]*(1-kt_list[line]))/(kt_list[line]*(1-k2_list[line]))              
                
            elif kt_list[line] > ktmax[line]:
                kd_model[line] = 1 - ((ktmax[line] * (1-kdmax[line]))/(kt_list[line]))
        
        """Variable hours"""
            
        if o3_list[line] > 0.0000001:
            """Define delta"""
            if 0.14 <= kt_list[line] <= kx_list[line]:
                delta = -3 * kl_list[line]**2 * (1 - kl_list[line]) * o3_list[line]**1.3
            
            elif kx_list[line] < kt_list[line] <= (kx_list[line] + 0.71):
                delta = 3 * kr_list[line] * (1 - kr_list[line])**2 * o3_list[line]**0.6
                
            elif kt_list[line] < 0.14:
                delta = 0
                
            elif kt_list[line] > (kx_list[line] + 0.71):
                delta = 0
                
            else:
                print('Line: ', line, ' k_t is: ', kt_list[line])
            
            """Now the old code with delta"""
            
            if kt_list[line] <= 0.22:
                kd_model[line] = 1 + delta
                
            elif 0.22 < kt_list[line] <= k2_list[line]:
                K = 0.5*(1+np.sin(np.pi*((kt_list[line]-0.22)/(k1_list[line]-0.22))-np.pi/2))
                
                kd_model[line] = 1 - (1-d1_list[line]) * (0.11*np.sqrt(K) + 0.15*K + 0.74*K**2) + delta
    
            elif k2_list[line] < kt_list[line] <= ktmax[line]:
                kd_model[line] = (d2_list[line]*k2_list[line]*(1-kt_list[line]))/(kt_list[line]*(1-k2_list[line])) + delta              
                
            elif kt_list[line] > ktmax[line]:
                kd_model[line] = 1 - ((ktmax[line] * (1-kdmax[line]))/(kt_list[line])) + delta
            
            
    
#Now again the correction for albedo
    if (np.abs(r - r_star) > 0.1):
        d_r_star = np.array(kd_model)
        d_r = 1 - (k_r_star * (1 - d_r_star) / k_r)
        
        kd_model = d_r
            
            
    
    
    
    
    if (np.abs(r - r_star) > 0.1):    
        filename = '%3s_skartveit_model_albedo.txt' % (place)
    else:
        filename = '%3s_skartveit_model.txt' % (place)
        
#Saving the data for further use        
    f = open(filename, 'w')
    print('#', file=f)
    print('#%7s %9s %9s %8s %9s %9s %9s %10s %9s %9s %9s %9s' % ('Date', 'Time', 'SH', 'I_EX', 'I_G', 'I_D', 'I_B', 'I_B2', 'k_t', 'k_d,meas', 'k_d,mod', 'o_3'), file=f)
    #print('#%18s %18s' % ('-'*15,'-'*15), file=f)
    for line in range(len(date_list)):
        output = '%8s %9i %9.3f %9.3f %9.3f %9.3f %9.2f %9.2f %9.6f %9.6f %9.5f %9.6f' % (date_list[line], time_list[line], sh_list[line], iex_list[line], ig_list[line], id_list[line], ib_list[line], ib2_list[line], kt_list[line], kd_list[line], kd_model[line], o3_list[line])
        print(output, file=f)
    
    f.close()
    print('File saved!')
    
            
    return