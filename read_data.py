#This script is for the second part of my bachelor thesis. It averages the stations measured data to one 'average' year 
#Then I calculate the diffuse radiation on a tilted plane using the PVLIB.python package.


import numpy as np
from datetime import datetime
from datetime import timedelta
import pvlib
import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
plt.ioff()
plt.rcParams.update({'font.size': 18})

#First, rad in the data. Converters are used to get datetime objects.
def read_data(file):
    if file not in ['Ny_Alesund','Eureka', 'Cape','Tiksi']:
        conv = {0: lambda x: datetime.strptime(x.decode("utf-8"),'%Y-%m-%d'),\
                1: lambda y: datetime.strptime(y.decode("utf-8"),'%H'), 2:np.float,\
                3:np.float, 4:np.float, 5:np.float, 6:np.float,7:np.float,8:np.float, \
                9:np.float,10:np.float,11:np.float}
    elif file == 'Bergen':
        conv = {0: lambda x: datetime.strptime(x.decode("utf-8"),'%Y-%m-%d'),\
                1: lambda y: datetime.strptime(y.decode("utf-8"),'%H'), 2:np.float,\
                3:np.float, 4:np.float, 5:np.float, 6:np.float,7:np.float,8:np.float}
    else:
        conv = {0: lambda x: datetime.strptime(x.decode("utf-8"),'%Y-%m-%d'),\
                1: lambda y: datetime.strptime(y.decode("utf-8"),'%H'), 2:np.float,\
                3:np.float, 4:np.float, 5:np.float, 6:np.float,7:np.float,8:np.float, \
                9:np.float}
    
    if file == 'Bergen':
        file_name = '6513-wm2_hourly.dat'
    else:
        file_name = '%2s_prepared.txt' % (file)
		
    imp = np.loadtxt('%2s' % (file_name), dtype=object, converters=conv, unpack=True)
    dates = []
    for i in range(len(imp[0])):
        emp = datetime(imp[0,i].year, imp[0,i].month, imp[0,i].day, imp[1,i].hour)
        dates.append(emp)
    imp[0,:] = dates
    imp = np.delete(imp, np.s_[1], axis=0)
    return imp

#Then I remove the 29th February, as it will have only a quarter of the data compared to the other stations.
def delete_feb(data):
    i = 0
    while i < len(data[0]):
        if (data[0,i].month == 2) and (data[0,i].day == 29):
            data = np.delete(data, np.s_[i], axis=1)
        else:
            i += 1
    return data    
        
#This function averages the data down to one year. The correction factor is for the 29th February as the datetime index still returns it in the function yday for the day of the year
def statistical_year(data,file):
    stat_year = np.zeros(shape=(len(data)+2,365*24),dtype=object)
    start = datetime.strptime("1900-01-01 00", "%Y-%m-%d %H")
    date_list = [start + timedelta(hours=x) for x in range(0,8760)]
    stat_year[0,:] = date_list
    for i in range(len(data[0])):
        if ((data[0,i].month > 2) and (( data[0,i].year % 400 == 0) or (( data[0,i].year % 4 == 0 ) and ( data[0,i].year % 100 != 0)))):
            correct = 24
        else:
            correct = 0
        index = (data[0,i].timetuple().tm_yday - 1)*24 + data[0,i].hour - correct
        stat_year[1:-2,index] = stat_year[1:-2,index] + data[1:,i]
        stat_year[-2,index] += 1
        
    stat_year[1:-2] = stat_year[1:-2] / stat_year[-2]
    
	#The Latitude and Longitude for every station to calculate the azimuth angle of the sun.
    lat_list = {'Lindenberg': 52.21,'Bergen': 60.39, 'Visby': 57.67, 'Norrkoping': 58.58, 'Kiruna': 67.84, 'Alert': 82.49, 'Barrow': 71.32, 'Cape': 79.27, 'Eureka': 79.99, 'Ny_Alesund': 78.93, 'Southpole': -89.98, 'Tiksi': 71.59, 'Neumayer': -70.65, 'Concordia': -75.10}
    lat = lat_list.get(file)
    
    long_list = {'Lindenberg': 14.122,'Bergen': 5.33, 'Visby': 18.28, 'Norrkoping': 16.20, 'Kiruna': 20.20, 'Alert': -62.42, 'Barrow': -156.607, 'Cape': 101.75, 'Eureka': -85.9404, 'Ny_Alesund': 11.93, 'Southpole': -24.799, 'Tiksi': 128.9188, 'Neumayer': -8.25, 'Concordia': 123.383}
    long = long_list.get(file)
    
	#The timezone for eery station to convert it to local time for the correct azimuth
    timezone_list = {'Lindenberg': 'Etc/GMT-1','Bergen': 'Etc/GMT-1', 'Visby': 'Etc/GMT-1', 'Norrkoping': 'Etc/GMT-1', 'Kiruna': 'Etc/GMT-1', 'Alert': 'Etc/GMT+5', 'Barrow': 'Etc/GMT+9', 'Cape': 'Etc/GMT-7', 'Eureka': 'Etc/GMT+6', 'Ny_Alesund': 'Etc/GMT-1', 'Southpole': 'Etc/GMT-13', 'Tiksi': 'Etc/GMT-9', 'Neumayer': 'UTC', 'Concordia': 'Etc/GMT-11'}
    timezone = timezone_list[file]
  
    timeindex = pd.to_datetime([start + timedelta(hours=x) + timedelta(minutes=30) for x in range(0,8760)])
    timeindex = timeindex.tz_localize('UTC')
    timeindex = timeindex.tz_convert(timezone)
    
    test = pvlib.solarposition.get_solarposition(timeindex,lat,long)
    azimuth = np.array(test['azimuth'])
    
    stat_year[-1] = azimuth
    
    return stat_year

#A quick summary function of the above definded functions
def do_all(file):
    A = read_data(file)
    A = delete_feb(A)
    A = statistical_year(A,file)
    return A

#The hay-model for the tilt
def tilt_hay(data,tilt,tilt_azi):
    #tilt in degrees
    #tilt_azi in degrees
    #data[1] = h
    #-1 = Azi
    #3 = Iex
    #4 = Ig
    #5 = Id
    #6 = Ib
    A = np.zeros(shape=(24*365))
    for i in range(len(A)):
        if (data[4,i] <= 0) or (data[5,i] <= 0) or (data[3,i] <= 0):
            A[i] = 0
        else:
            A[i] = pvlib.irradiance.haydavies(tilt,tilt_azi, data[5,i], data[6,i], data[3,i], 90-data[1,i],data[-1,i])

    return A

#The Perez tilt model
def tilt_perez(data,tilt,tilt_azi):
    A = np.zeros(shape=(24*365))

    for i in range(len(A)):
        airmass = pvlib.atmosphere.get_relative_airmass(90-data[1,i])
        if (data[4,i] <= 0) or (data[5,i] <= 0) or (data[3,i] <= 0):
            A[i] = 0
        else:
            A[i] = pvlib.irradiance.perez(tilt,tilt_azi, data[5,i], data[6,i], data[3,i], 90-data[1,i],data[-1,i],airmass)

    return A

#The Klucher-model for tilted diffuse radiation
def tilt_klucher(data, tilt, tilt_azi):
    
    A = np.zeros(shape=(24*365))
    for i in range(len(A)):
        if (data[4,i] <= 0) or (data[5,i] <= 0) or (data[3,i] <= 0):
            A[i] = 0
        else:
            A[i] = pvlib.irradiance.klucher(tilt, tilt_azi, data[5,i], data[4,i], 90-data[1,i], data[-1,i])

    return A

#The reindl model for tilted radiation
def tilt_reindl(data, tilt, tilt_azi):
    
    A = np.zeros(shape=(24*365))
    for i in range(len(A)):
        if (data[4,i] <= 0) or (data[5,i] <= 0) or (data[3,i] <= 0):
            A[i] = 0
        else:
            A[i] = pvlib.irradiance.reindl(tilt, tilt_azi, data[5,i], data[6,i], data[4,i], data[3,i], 90-data[1,i], data[-1,i])

    return A

#This function calculates the direct beam radiation on a tilted plane
def get_ib_tilt(data, tilt, tilt_az):
    tilt = tilt*np.pi/180
    tilt_az = tilt_az*np.pi/180
    h = (np.piecewise(data[1], [data[1] < 0, data[1] >= 0], [0, lambda x: x*np.pi/180])).astype(float)
    s_az = (data[-1]*np.pi/180).astype(float)
    Ib_tilt = data[6] * (np.sin(tilt) * np.cos(h) * np.cos(tilt_az - s_az) + np.cos(tilt) * np.sin(h))

    return Ib_tilt

#These function combine the tilted diffuse models with the tilted direct beam
def get_ig_tilt_hay(data, tilt, tilt_azi):
        
    Id_tilt = tilt_hay(data, tilt, tilt_azi)
    Ib_tilt = get_ib_tilt(data, tilt, tilt_azi)
    Ig_tilt = Id_tilt + Ib_tilt
    
    return Ig_tilt

def get_ig_tilt_perez(data, tilt, tilt_azi):
    
    Id_tilt = tilt_perez(data, tilt, tilt_azi)
    Ib_tilt = get_ib_tilt(data, tilt, tilt_azi)
    Ig_tilt = Id_tilt + Ib_tilt
    
    return Ig_tilt
    
def get_ig_tilt_klucher(data, tilt, tilt_azi):
    
    Id_tilt = tilt_klucher(data, tilt, tilt_azi)
    Ib_tilt = get_ib_tilt(data, tilt, tilt_azi)
    Ig_tilt = Id_tilt + Ib_tilt

    return Ig_tilt

def get_ig_tilt_reindl(data, tilt, tilt_azi):
    
    Id_tilt = tilt_reindl(data, tilt, tilt_azi)
    Ib_tilt = get_ib_tilt(data, tilt, tilt_azi)
    Ig_tilt = Id_tilt + Ib_tilt
    
    return Ig_tilt

#If desired, this gives the ground reflected global radiation. The Albedo is needed.
def get_ig_ground_reflected(data,tilt,albedo):
    tilt = tilt*np.pi/180
    Ig_ground = data[4] * albedo * np.sin(tilt/2)**2
    
    return Ig_ground

#This function averages one year of tilted global radiation to one value. 
def average(Ig_tilt):
    return (sum(Ig_tilt[Ig_tilt > 0]/len(Ig_tilt[Ig_tilt > 0])))

#This calculates the tilted global radiation for all possible tilt and azimuth angles in 5 and 10 degrees steps respectively.
def find_best_angles(file,model):
    data = do_all(file)
    funcs = {'hay':get_ig_tilt_hay, 'perez':get_ig_tilt_perez, 'klucher':get_ig_tilt_klucher, 'reindl':get_ig_tilt_reindl}
    albedos = {'Lindenberg': 0.2,'Bergen': 0.22, 'Visby': 0.2, 'Norrkoping': 0.2, 'Kiruna': 0.25, 'Alert': 0.3, 'Barrow': 0.3, 'Cape': 0.3, 'Eureka': 0.3, 'Ny_Alesund': 0.3, 'Southpole': 0.5, 'Tiksi': 0.3, 'Neumayer': 0.5, 'Concordia': 0.5}
    f = funcs[model]
    albedo = albedos[file]
    f_2 = get_ig_ground_reflected
    
    global tilts
    global tilts_azi
    
	#Define the stepsize here
    tilts = np.arange(0,91,5)    
    tilts_azi = np.arange(0,361,10)
    
    results = np.zeros(shape=(len(tilts),len(tilts_azi)))
    results_ib = np.zeros(shape=(len(tilts),len(tilts_azi)))
    results_gr = np.zeros(shape=(len(tilts),len(tilts_azi)))
    #l is just a counter for console output of the current progress
	l = 1
    #Here you can choose between including the ground reflected component or not.
    mod = 'ground'
    
	#The problem with the klucher model were some incredibly high values. Therefore all above 1000 will be filtered out.
    if mod == 'all':
        for i in range(len(tilts)):
            for k in range(len(tilts_azi)):
                results[i,k] = average(f(data, tilts[i], tilts_azi[k])+f_2(data,tilts[i],albedo))
                results_ib[i,k] = average(get_ib_tilt(data, tilts[i], tilts_azi[k]))
                if model == 'klucher':
                    if results[i,k] > 1000:
                        results[i,k] = 0
                if k == len(tilts_azi)-1:
                    print(np.round(l/len(tilts),decimals=2))
                    l += 1
                    
    elif mod == 'ground':
        for i in range(len(tilts)):
            for k in range(len(tilts_azi)):
                results_gr[i,k] = average(f_2(data,tilts[i],albedo))
                if model == 'klucher':
                    if results[i,k] > 1000:
                        results[i,k] = 0
                if k == len(tilts_azi)-1:
                    print(np.round(l/len(tilts),decimals=2))
                    l += 1
            
    return results, results_ib, results_gr

import scipy.interpolate

#The plot function
def colormap_plot(result,result_ib,place,model):
	#First we interpolate the data onto one degree steps
    y_i = np.arange(0,91,1)
    x_i = np.arange(0,360,1)
   
    result_inter = scipy.interpolate.interp2d(tilts_azi, tilts, result)
    result_inter_ib = scipy.interpolate.interp2d(tilts_azi, tilts, result_ib)
    z_i = result_inter(x_i,y_i)
    z_i2 = result_inter_ib(x_i,y_i)
    
	#The plots. Colormap plots with automatic colorbar on the side and contours at 92.5%, 75% and 50% of the maximum value.
	#ct2 gives the comparison with tilted direct beam radiation.
    a = plt.figure(figsize=(20,15))
    plt.imshow(z_i,cmap=plt.get_cmap('afmhot'), vmin=result.min()-20, vmax=result.max()+20, origin='lower', extent=[tilts_azi.min(), tilts_azi.max(), tilts.min(), tilts.max()])
    plt.colorbar(shrink=0.4, aspect=10)
    ct = plt.contour(x_i, y_i, z_i, [result.max()-(result.max()/2), result.max()-(result.max()/4), result.max()-(result.max()/(13+1/3))], colors='darkgreen')
    ct2 = plt.contour(x_i, y_i, z_i2, [result_ib.max()-(result_ib.max()/2), result_ib.max()-(result_ib.max()/4), result_ib.max()-(result_ib.max()/(13+1/3))], colors='darkblue')
    plt.clabel(ct,fmt='%d',colors='darkgreen',inline_spacing=30.)
    plt.xlabel('tilt azimuth')
    plt.ylabel('tilt angle')
    plt.title('%2s with %2s' % (place, model))
    plt.show()
    a.savefig("%2s_%2s_colormap_ground_reflected.png" % (place,model), bbox_inches='tight')

    return

#This can save the modelled values. Not really used for anything else. The modelling can take some time, so saving may be useful for coming back later.
def save_results(result,place,model):
    filename = 'tilted_%2s_%2s.txt' % (place,model)
        
    f = open(filename, 'w')
    length_y = np.arange(len(result)-1,-1,-1)
    length_x = np.arange(0,len(result[0]),1)
    
    for i in length_y:
        output = '%3s ' % (tilts[i])
        for k in length_x:
            output += '& %8.2f ' % (result[i,k])
        print(output + '\\\\', file=f)
        
    output = '    '
    for k in length_x:
        output += '& %8.2f ' % (tilts_azi[k])
        
    print(output,file=f)
    
    f.close()
    print('File saved!')