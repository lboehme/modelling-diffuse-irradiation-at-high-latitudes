#This script is the implementation of the BRL-model. Again, more numpy would quicken things up.

import numpy as np

def model_BRL(place, model):
	#Set the filename according to the station and then read in the data
    if place == 'Bergen':
        filename = '6513-wm2_hourly.dat'
    elif place in ['Kiruna', 'Norrkoping','Visby', 'Ny_Alesund', 'Alert', 'Barrow', 'Cape', 'Eureka','Tiksi','Southpole','Lindenberg','Neumayer','Concordia']:
        filename = '%3s_prepared.txt' % (place)
    else:
        raise ValueError
    #print('Takk!')
    
    f = open(filename, 'r')
    lines = f.readlines()
    f.close()
    
	#Since I also included the BRL-H-L variant here, the Latitude is needed for later.
    lat_list = {'Lindenberg': 52.21,'Bergen': 60.39, 'Visby': 57.67, 'Norrkoping': 58.58, 'Kiruna': 67.84, 'Alert': 82.49, 'Barrow': 71.32, 'Cape': 79.27, 'Eureka': 79.99, 'Ny_Alesund': 78.93, 'Southpole': 89.98, 'Tiksi': 71.59, 'Neumayer': 70.65, 'Concordia': 75.10}
    lat = lat_list.get(place)*np.pi/180
	
	#Check for spelling errors in the model
    if model in ['Fit', 'Bay', 'Min', 'Min2','Min3','Lat']:
        pass
    else:
        raise ValueError
    
    
    # Make empty lists
    if place == 'Bergen':
        date_list = ['19650401']
        time_list = [0]
        sh_list = [0]
        az_list = [0]
        iex_list = [0]
        ig_list = [0]
        id_list = [0]
        ib_list = [0]
        ib2_list = [0]
        AST_list = [0]
    if place == 'Ny_Alesund':
        date_list = ['1993-01-01']
        time_list = [00]
        sh_list = [0]
        az_list = [0]
        iex_list = [0]
        ig_list = [0]
        id_list = [0]
        ib_list = [0]
        ib2_list = [0]
        AST_list = [0.22048]
    else:
        date_list = []
        time_list = []
        sh_list = []
        az_list = []
        iex_list = []
        ig_list = []
        id_list = []
        ib_list = []
        ib2_list = []
        AST_list = []
    
    #Fill lists with data
    for line in lines:
        if line.startswith('#'):
            continue
        s = line.split()
        date_list.append(str(s[0]))
        time_list.append(int(s[1]))
        sh_list.append(float(s[2]))
        az_list.append(float(s[3]))
        iex_list.append(float(s[4]))
        ig_list.append(float(s[5]))
        id_list.append(float(s[6]))
        ib_list.append(float(s[7]))
        ib2_list.append(float(s[8]))
        if place == 'Bergen':
            AST_list.append(float(s[1])%24)
        else:
            AST_list.append(float(s[9])%24)
    
    kd_list = [None] * len(date_list)
    kt_list = [None] * len(date_list)     
    per_list = [None] * len(date_list) 
    KT_list = [None] * len(date_list)
    kd_model = [None] * len(kd_list)
    
       
    #Calculate k_t and k_d and fill in their list
    """k_t"""
    for line in range(len(ig_list)):
        try: 
            kt_list[line] = (ig_list[line]/iex_list[line])
        except ZeroDivisionError:
            kt_list[line] = 0
            continue
    
    """k_d"""
    for line in range(len(date_list)):
        try: 
            kd_list[line] = (id_list[line]/ig_list[line])
        except ZeroDivisionError:
            kd_list[line] = 0
            continue
      
    """Persistence"""
    per_list[0] = 0
    per_list[len(date_list)-1] = 0
    for line in range(1,len(date_list)-1):
        if (ig_list[line-1] == 0) and (ig_list[line] > 0):
            per_list[line] = kt_list[line+1]
        elif (ig_list[line+1] == 0) and (ig_list[line] > 0):
            per_list[line] = kt_list[line-1]
        elif (ig_list[line-1] > 0) and (ig_list[line+1] > 0):
            per_list[line] = (kt_list[line-1] + kt_list[line+1])/2
        else:
            per_list[line] = 0
            
    """Daily clearness"""
	#Calculate the Daily Clearness factor. The difficult part is that it is based on AST, and that there are some jumps between days. So it may jump from 23:58 to 01:01 because of the equation of time.
    #Also have to check for the beginning, as the first measurement may not start at 00:00
	if AST_list[0] >= 1:
        for i in range(24):
            if AST_list[i] < 1:
                ending = i
                break
        for i in range(ending):
            try: 
                KT_list[i] = sum(ig_list[0:ending])/sum(iex_list[0:ending])
            except ZeroDivisionError:
                KT_list[i] = 0
    for line in range(len(date_list)):
        if AST_list[line] <= 1.01:
            try:
                KT = sum(ig_list[line:line+24])/sum(iex_list[line:line+24])
            except ZeroDivisionError:
                KT = 0
            for i in range(24):
                try:
                    KT_list[line+i] = KT
                except IndexError:
                    continue
    
    
    #Get the parameters for the chosen BRL-model
    if model == 'Fit':
        """Least Square Fit"""
        b0 = -5.38
        b1 = 6.63
        b2 = 0.006
        b3 = -0.007
        b4 = 1.75
        b5 = 1.31
        
    elif model == 'Bay':
        b0 = -5.32
        b1 = 7.28
        b2 = -0.003
        b3 = -0.0047
        b4 = 1.72
        b5 = 1.08
        
    elif model == 'Lat':
        b = [-3.19770206,  6.67977243,  1.61676494,  1.06015204, -3.67943995, -1.00434543,  0.03328069,  1.64218568,  1.64891401, -6.12479022, -5.49821597,  0.61535358,  3.47724122, -2.04517801,  1.64311886, 2.23147351]
    
#Define the model
    if model in ['Bay','Min','Fit','Min2']:
        for line in range(len(kd_list)):
            kd_model[line] = 1 / (1 + np.exp(b0 + b1 * kt_list[line] + b2 * AST_list[line] + b3 * sh_list[line] + b4 * KT_list[line] + b5 * per_list[line]))
                    
    elif model == 'Lat':
        for k in range(len(kd_list)):
            if 0.25 < kt_list[k] <= 0.8:
                kd_model[k] = 1/(1 + np.exp(b[0] + b[1] * kt_list[k] + b[2] * KT_list[k] + b[3] * per_list[k] + b[13] * lat))
            elif 0.8 < kt_list[k]:
                kd_model[k] = 1/(1 + np.exp(b[4] + b[5] * kt_list[k] + b[6] * sh_list[k] + b[7] * KT_list[k] + b[8] * per_list[k] + b[14] * lat))
            elif kt_list[k] <= 0.25:
                kd_model[k] = 1/(1 + np.exp(b[9] + b[10] * kt_list[k] + b[11] * KT_list[k] + b[12] * per_list[k] + b[15] * lat))

   
	#Save the data for further use (plotting)
    filename = '%3s_BRL_model_%3s.txt' % (place, model)
    f = open(filename, 'w')
    print('#%7s %9s %9s %9s %9s %9s %10s %9s %9s %9s %9s %9s %9s' % ('Date', 'Time', 'SH', 'I_EX', 'I_G', 'I_D', 'I_B', 'I_B2', 'k_t', 'k_d,meas', 'k_d,mod', 'Per', 'KT'), file=f)

    for line in range(len(date_list)):
        output = '%8s %9i %9.3f %9.2f %9.2f %9.2f %10.2f %9.2f %9.5f %9.5f %9.5f %9.5f %9.5f' % (date_list[line], time_list[line], sh_list[line], iex_list[line], ig_list[line], id_list[line], ib_list[line], ib2_list[line], kt_list[line], kd_list[line], kd_model[line], per_list[line], KT_list[line])
        print(output, file=f)
    
    f.close()
    print('File saved!')
    
    
    
	return